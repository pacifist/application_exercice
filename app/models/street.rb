class Street < ApplicationRecord
  has_many :commune_streets
  has_many :communes, through: :commune_streets

  validates_presence_of :title
  validates_numericality_of :from, only_integer: true, allow_nil: true
  validates_numericality_of :to, only_integer: true, allow_nil: true,
                                 greater_than: -> (street) { street.from }
end
