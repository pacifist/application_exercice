class Commune < ApplicationRecord
  belongs_to :intercommunality, optional: true
  has_many :commune_streets
  has_many :streets, through: :commune_streets

  validates_presence_of :name, :code_insee
  validates_format_of :code_insee, with: /\A\d{5}\z/

  def self.search(name)
    where('LOWER(name) LIKE ?', "%#{sanitize_sql_like(name.to_s.downcase)}%")
  end

  def self.to_hash
    Hash[all.map{ |c| [c.code_insee, c.name] }]
  end
end
