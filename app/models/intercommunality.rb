class Intercommunality < ApplicationRecord
  has_many :communes

  validates_presence_of :name, :siren
  validates_uniqueness_of :siren, case_sensitive: false
  validates_format_of :siren, with: /\A\d{9}\z/
  validates_inclusion_of :form, in: %w(ca cu cc met)

  before_save :generate_slug

  def communes_hash
    Hash[communes.map{ |c| [c.code_insee, c.name] }]
  end

  private

    def generate_slug
      if self.name.blank? || self.slug.present?
        return
      end

      self.slug = self.name.strip.to_slug.transliterate.to_s.downcase.gsub(/\s+/, '-')
    end
end
