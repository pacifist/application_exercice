require 'csv'

class ImportJob < ApplicationJob
  queue_as :default

  def perform(csv_path)
    CSV.foreach(csv_path, encoding: 'windows-1252', headers: true, col_sep: ';') do |row|
      intercommunality = Intercommunality.where(siren: row['siren_epci']).first_or_initialize
      commune = Commune.where(code_insee: row['insee']).first_or_initialize

      if intercommunality.new_record?
        intercommunality.name = row['nom_complet']
        intercommunality.form = row['form_epci'][0..2].downcase
        intercommunality.save!
      end

      if commune.new_record?
        commune.name = row['nom_com']
        commune.population = row['pop_total']
        commune.intercommunality = intercommunality
        commune.save!

        intercommunality.population = intercommunality.population.to_i + commune.population
        intercommunality.save!
      end
    end
  end
end
