class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  rescue_from ActionController::UnknownFormat, with: :handle_unknown_format

  private

    def handle_unknown_format
      head :not_acceptable
    end
end
