class CommunesController < ApplicationController
  before_action :find_commune, only: [:show, :update]

  def index
    respond_to do |format|
      format.json do
        render json: Commune.all
      end
    end
  end

  def create
    head :forbidden
  end

  def show
    render json: @commune
  end

  def update
    commune_params = params.fetch(:commune, {}).permit(:name)
    if commune_params.empty?
      return head :bad_request
    end

    @commune.update_attributes!(commune_params)
    head :no_content
  end

  private

    def find_commune
      @commune = Commune.find_by_code_insee(params[:id])
      if @commune.nil?
        head :not_found
      end
    end
end