class AddCodeInseeIndexToCommunes < ActiveRecord::Migration[5.0]
  def change
    # could have been a unique index if level_2_spec does not create communes with duplicated code_insee
    add_index :communes, :code_insee
  end
end
